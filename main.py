"""This is the main entrypoint of this project.
    When executed as
    python main.py input_folder/
    it should perform the neural network inference on all images in the `input_folder/` and print the results.
"""

from argparse import ArgumentParser
import sys

class_names = ['daisy', 'dandelion', 'roses', 'sunflowers', 'tulips']

def parse_args():
    """Define CLI arguments and return them.

    Feel free to modify this function if needed.

    Returns:
        Namespace: parser arguments
    """
    parser = ArgumentParser()
    parser.add_argument("input_folder", type=str, help="A folder with images to analyze.")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    cli_args = parse_args()
    # do stuff ...
    print(f"Analyzing folder : {cli_args.input_folder}", file=sys.stderr)  # info print must be done on stderr like this for messages

    # print results on stdout
    print({"image.jpg":{"score":0.25, "class":"tulip"}})