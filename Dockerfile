# The Docker container build file

# base image, feel free to change it
FROM scratch

COPY . .

# What is executed when calling the docker container
ENTRYPOINT [ "python", "main.py" ]